package com.example;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.RequestBuilder;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@WebMvcTest(WordsController.class)
public class WordsControllerTest {

    @Autowired
    private MockMvc mvc;

    @Test
    public void testCamelizeWithDefault() throws Exception {
        mvc.perform(get("/camelize?original=foo_bar_baz"))
                .andExpect(status().isOk())
                .andExpect(content().string("fooBarBaz"));
    }

    @Test
    public void testCamelizeWithInitialCap() throws Exception {
        mvc.perform(get("/camelize?original=foo_bar_baz&initialCap=true"))
                .andExpect(status().isOk())
                .andExpect(content().string("FooBarBaz"));
    }

    @Test
    public void testRedact() throws Exception {
        mvc.perform(get("/redact?original=A little of this and a little of that&badWord=little&badWord=this"))
                .andExpect(status().isOk())
                .andExpect(content().string("A ****** of **** and a ****** of that"));
    }

    @Test
    public void testEncode() throws Exception {
        RequestBuilder request = post("/encode")
                .param("message", "a little of this and a little of that")
                .param("key", "mcxrstunopqabyzdefghijklvw");

        mvc.perform(request)
                .andExpect(status().isOk())
                .andExpect(content().string("m aohhas zt hnog myr m aohhas zt hnmh"));
    }

    @Test
    public void testSed() throws Exception {
        RequestBuilder request = post("/s/Love/Death")
                .contentType(MediaType.TEXT_PLAIN)
                .content("Love doesn't discriminate");

        mvc.perform(request)
                .andExpect(status().isOk())
                .andExpect(content().string("Death doesn't discriminate"));
    }

}
